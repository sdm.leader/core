﻿using Flurl;
using Flurl.Http;
using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Core.Service.Http
{
    public sealed class NotificationService : INotificationService
    {
        private readonly string _serviceUrl;

        public NotificationService(string serviceUrl) 
        { 
            _serviceUrl = serviceUrl ?? throw new ArgumentNullException(nameof(serviceUrl));
        }

        public Task<NotificationDTO?> GetAsync(long id, CancellationToken cancellationToken)
        {
            return _serviceUrl
                .AppendPathSegments("admin/notification", id)
                .GetJsonAsync<NotificationDTO?>(cancellationToken);
        }

        public void Create(NotificationDTO notification, CancellationToken cancellationToken)
        {
            _serviceUrl
                .AppendPathSegment("admin/notification/create")
                .PostJsonAsync(notification, cancellationToken)
                .Wait(cancellationToken);
        }        
    }
}