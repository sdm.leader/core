﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Core.Service
{
    public interface INotificationSenderService
    {
        Task SendAsync(UserDTO user, NotificationDTO notification, CancellationToken cancellationToken);
    }
}
