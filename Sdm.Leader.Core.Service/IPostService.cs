﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Core.Service
{
    public interface IPostService
    {
        void Create(PostDTO post, CancellationToken cancellationToken);
    }
}
