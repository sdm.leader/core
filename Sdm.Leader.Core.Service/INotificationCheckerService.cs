﻿namespace Sdm.Leader.Core.Service
{
    public interface INotificationCheckerService
    {
        void Check(CancellationToken cancellationToken);

        void Stop(CancellationToken cancellationToken);
    }
}
