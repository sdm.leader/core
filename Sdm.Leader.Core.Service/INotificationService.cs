﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Core.Service
{
    public interface INotificationService
    {
        Task<NotificationDTO?> GetAsync(long id, CancellationToken cancellationToken);

        void Create(NotificationDTO notification, CancellationToken cancellationToken);
    }
}
