﻿namespace Sdm.Leader.Core.DTO
{
    public class UserDTO
    {
        public long Id { get; set; }

        public int LevelId { get; set; }

        public UserRole? Role { get; set; }

        public string Name { get; set; } = null!;
    }
}
