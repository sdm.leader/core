﻿namespace Sdm.Leader.Core.DTO
{
    public class PostDTO
    {
        public long Id { get; set; }

        public string? Data { get; set; }
    }
}
