﻿namespace Sdm.Leader.Core.DTO
{
    public enum UserRole
    {
        Administrator,
        Distributor,
        Marketer,
        Sales
    }
}
